# Selfish
## A short screenplay by Ofek Doitch

This screenplay is offered as an inside view to film development at Weedon & Scott. All rights reserver to the original author, Ofek Doitch.

### Copyright Notice

Copyright   ©  � �2020 by Ofek Doitch

All rights reserved. No part of this repository or any of the files in it may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, or other electronic or mechanical methods, without the prior written permission of the publisher, except in the case of brief quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For permission requests, write to copyright {[at]} weedonandscott {[dot]} com.
